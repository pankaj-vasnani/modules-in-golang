package calc

func Add(args ...int) int {
	s := 0

	for _, val := range args {
		s += val
	}

	return s
}

func Sub(a, b int) int {
	return a - b
}
